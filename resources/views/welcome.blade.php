<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.cdnfonts.com/css/georgia" rel="stylesheet">
        <link rel="preconnect" href="https://fonts.bunny.net">
        <link href="https://fonts.bunny.net/css?family=figtree:400,600&display=swap" rel="stylesheet" />
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
        <link rel="stylesheet" href="{{asset('/css/style.css')}}">
    </head>
    <body>
        <div class="container-fluid p-0 m-0">
            <nav id="navbar" class="navbar fixed-top navbar-expand-lg navbar-light border-bottom border-dark py-3 px-5">
                <a class="navbar-brand pl-5 font-weight-bold" href="#">Publisher</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ms-auto pr-5">
                        <li class="nav-item active px-2">
                            <a class="nav-link" href="#">Our Story <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item px-2">
                            <a class="nav-link text-dark" href="#">Membership</a>
                        </li>
                        <li class="nav-item px-2">
                            <a class="nav-link text-dark" href="#">Write</a>
                        </li>
                        <li class="nav-item px-2">
                            <a class="nav-link text-dark" href="{{route('login')}}">Sign in</a>
                        </li>
                        <li class="nav-item px-2 text-white border-0 rounded-pill">
                            <a class="nav-link text-white" id="get-started" href="#">Get Started</a>
                        </li>
                    </ul>
                </div>
            </nav>
            <section class="h-auto bg-warning mt-5 heading-text border-bottom border-dark">
                <h1 class="">Stay curious.</h1>
                <p>Discover stories, thinking, and expertise from writers on any topic.</p>
                <a class=" px-5 py-2 text-white bg-dark text-white border-0 rounded-pill" href="#">Start Reading</a>
            </section>

            <section class="p-5  border-bottom border-gray">
                <h3 class="py-2 text-center">Trending Post</h3>
                <div class="row">
                    <div class="col-md-4">
                        <h5>Heading</h5>
                        <p class="text-justify">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque esse eveniet illo, impedit in, ipsum laudantium molestias nam quidem quo quod quos repudiandae rerum sequi soluta totam unde. Accusamus, fugiat!</p>
                    </div>
                    <div class="col-md-4">
                        <h5>Heading</h5>
                        <p class="text-justify">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque esse eveniet illo, impedit in, ipsum laudantium molestias nam quidem quo quod quos repudiandae rerum sequi soluta totam unde. Accusamus, fugiat!</p>
                    </div>
                    <div class="col-md-4">
                        <h5>Heading</h5>
                        <p class="text-justify">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque esse eveniet illo, impedit in, ipsum laudantium molestias nam quidem quo quod quos repudiandae rerum sequi soluta totam unde. Accusamus, fugiat!</p>
                    </div>
                    <div class="col-md-4">
                        <h5>Heading</h5>
                        <p class="text-justify">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque esse eveniet illo, impedit in, ipsum laudantium molestias nam quidem quo quod quos repudiandae rerum sequi soluta totam unde. Accusamus, fugiat!</p>
                    </div>
                    <div class="col-md-4">
                        <h5>Heading</h5>
                        <p class="text-justify">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque esse eveniet illo, impedit in, ipsum laudantium molestias nam quidem quo quod quos repudiandae rerum sequi soluta totam unde. Accusamus, fugiat!</p>
                    </div>
                    <div class="col-md-4">
                        <h5>Heading</h5>
                        <p class="text-justify">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque esse eveniet illo, impedit in, ipsum laudantium molestias nam quidem quo quod quos repudiandae rerum sequi soluta totam unde. Accusamus, fugiat!</p>
                    </div>
                </div>
            </section>
            <section class="p-5">
                <div class="row">
                    <div class="col-md-7 col-lg-7 pb-3">
                        <div class="row justify-content-center">
                            <div class="col-md-8">
                                <span>Author name</span>
                                <h4>Heading</h4>
                                <p class="text-justify mb-0">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque esse eveniet illo, impedit in, ipsum laudantium molestias nam quidem quo quod.</p>
                                <p class="p-0 mt-2 text-secondary">Mar 6 - 6 min</p>
                            </div>
                            <div class="col-md-4 py-4">
                                <img src="{{asset('/img/0_w1thsdt65_IqeSoI.jpg')}}" width="200" height="140" alt="">
                            </div>
                        </div>
                        <div class="row justify-content-center">
                            <div class="col-md-8">
                                <span>Author name</span>
                                <h4>Heading</h4>
                                <p class="text-justify mb-0">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque esse eveniet illo, impedit in, ipsum laudantium molestias nam quidem quo quod.</p>
                                <p class="p-0 mt-2 text-secondary">Mar 6 - 6 min</p>
                            </div>
                            <div class="col-md-4 py-4">
                                <img src="{{asset('/img/0_w1thsdt65_IqeSoI.jpg')}}" width="200" height="140" alt="">
                            </div>
                        </div>
                        <div class="row justify-content-center">
                            <div class="col-md-8">
                                <span>Author name</span>
                                <h4>Heading</h4>
                                <p class="text-justify mb-0">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque esse eveniet illo, impedit in, ipsum laudantium molestias nam quidem quo quod.</p>
                                <p class="p-0 mt-2 text-secondary">Mar 6 - 6 min</p>
                            </div>
                            <div class="col-md-4 py-4">
                                <img src="{{asset('/img/0_w1thsdt65_IqeSoI.jpg')}}" width="200" height="140" alt="">
                            </div>
                        </div>
                        <div class="row justify-content-center">
                            <div class="col-md-8">
                                <span>Author name</span>
                                <h4>Heading</h4>
                                <p class="text-justify mb-0">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque esse eveniet illo, impedit in, ipsum laudantium molestias nam quidem quo quod.</p>
                                <p class="p-0 mt-2 text-secondary">Mar 6 - 6 min</p>
                            </div>
                            <div class="col-md-4 py-4">
                                <img src="{{asset('/img/0_w1thsdt65_IqeSoI.jpg')}}" width="200" height="140" alt="">
                            </div>
                        </div>
                        <div class="row justify-content-center">
                            <div class="col-md-8">
                                <span>Author name</span>
                                <h4>Heading</h4>
                                <p class="text-justify mb-0">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque esse eveniet illo, impedit in, ipsum laudantium molestias nam quidem quo quod.</p>
                                <p class="p-0 mt-2 text-secondary">Mar 6 - 6 min</p>
                            </div>
                            <div class="col-md-4 py-4">
                                <img src="{{asset('/img/0_w1thsdt65_IqeSoI.jpg')}}" width="200" height="140" alt="">
                            </div>
                        </div>
                        <div class="row justify-content-center">
                            <div class="col-md-8">
                                <span>Author name</span>
                                <h4>Heading</h4>
                                <p class="text-justify mb-0">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque esse eveniet illo, impedit in, ipsum laudantium molestias nam quidem quo quod.</p>
                                <p class="p-0 mt-2 text-secondary">Mar 6 - 6 min</p>
                            </div>
                            <div class="col-md-4 py-4">
                                <img src="{{asset('/img/0_w1thsdt65_IqeSoI.jpg')}}" width="200" height="140" alt="">
                            </div>
                        </div>
                        <div class="row justify-content-center">
                            <div class="col-md-8">
                                <span>Author name</span>
                                <h4>Heading</h4>
                                <p class="text-justify mb-0">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque esse eveniet illo, impedit in, ipsum laudantium molestias nam quidem quo quod.</p>
                                <p class="p-0 mt-2 text-secondary">Mar 6 - 6 min</p>
                            </div>
                            <div class="col-md-4 py-4">
                                <img src="{{asset('/img/0_w1thsdt65_IqeSoI.jpg')}}" width="200" height="140" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="sidebar col-md-5 col-lg-5">
                        <div class="footer">
                            <h5>Discover more of what matters to you</h5>
                            <div class="links">
                                <a href="" class="btn btn-transparent border border-gray my-1" >Programming</a>
                                <a href="" class="btn btn-transparent border border-gray my-1" >Data Science</a>
                                <a href="" class="btn btn-transparent border border-gray my-1" >Technology</a>
                                <a href="" class="btn btn-transparent border border-gray my-1" >Self Improvement</a>
                                <a href="" class="btn btn-transparent border border-gray my-1" >Writing</a>
                                <a href="" class="btn btn-transparent border border-gray my-1" >Relationship</a>
                                <a href="" class="btn btn-transparent border border-gray my-1" >Machine Learning</a>
                                <a href="" class="btn btn-transparent border border-gray my-1" >Machine Learning</a>
                                <a href="" class="btn btn-transparent border border-gray my-1" >Productivity</a>
                                <a href="" class="btn btn-transparent border border-gray my-1" >Politics</a>
                            </div>
                        </div>
                        <div class="border-bottom border-gray py-3" ></div>
                        <div class="footer-menu pt-2">
                            <a href="" class="btn btn-transparent">Help</a>
                            <a href="" class="btn btn-transparent">Status</a>
                            <a href="" class="btn btn-transparent">Writers</a>
                            <a href="" class="btn btn-transparent">Blog</a>
                            <a href="" class="btn btn-transparent">Careers</a>
                            <a href="" class="btn btn-transparent">Privacy</a>
                            <a href="" class="btn btn-transparent">Teams</a>
                            <a href="" class="btn btn-transparent">About</a>
                            <a href="" class="btn btn-transparent">Text to speech</a>
                        </div>
                    </div>
                </div>
            </section>
        </div>


    <script src="{{asset('/js/custom.js')}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js" integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js" integrity="sha384-oBqDVmMz9ATKxIep9tiCxS/Z9fNfEXiDAYTujMAeBAsjFuCZSmKbSSUnQlmh/jp3" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.min.js" integrity="sha384-mQ93GR66B00ZXjt0YO5KlohRA5SY2XofN4zfuZxLkoj1gXtW8ANNCe9d5Y3eG5eD" crossorigin="anonymous"></script>
    </body>
</html>
